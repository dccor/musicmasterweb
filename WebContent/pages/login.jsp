<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MusicMaster-Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link rel="icon" type="image/png"
	href="${pageContext.request.contextPath}/webresources/images/icons/favicon.ico" />
<!--===============================================================================================-->
<!-- <link rel="stylesheet" type="text/css" -->
<%-- 	href="${pageContext.request.contextPath}/webresources/Login/vendor/bootstrap/css/bootstrap.min.css"> --%>
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/webresources/css/font-awesome.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/webresources/Login/vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/webresources/Login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/webresources/Login/vendor/select2/select2.min.css">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/webresources/css/login.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/webresources/css/util.css">
</head>



<body>
<%@ page import = "java.sql.*" %>
<%@ page import="javax.sql.*" %>

	<div class="overlay">
		<div id="header">
			<h1 class="h1">
				<b>Welcome to Music Master</b>
			</h1>
			<hr />
			<h2>developing stage ...</h2>
		</div>


		<div class="limiter">
			<div class="container-login100">
				<div class="wrap-login100">
					<div class="login100-pic js-tilt" data-tilt>
						<img
							src="${pageContext.request.contextPath}/webresources/images/logo.png"
							alt="IMG">
					</div>

					<form class="login100-form validate-form">
						<span class="login100-form-title"> Member Login </span>
						
						<div class="wrap-input100 validate-input"
							data-validate="Username is required">
							<input class="input100" type="username" name="username"
								placeholder="Username"> <span class="focus-input100"></span>
							<span class="symbol-input100"> <i class="fa fa-lock"
								aria-hidden="true"></i>
							</span>
						</div>

<!-- 						<div class="wrap-input100 validate-input" -->
<!-- 							data-validate="Valid email is required: ex@abc.xyz"> -->
<!-- 							<input class="input100" type="text" name="email" -->
<!-- 								placeholder="Email"> <span class="focus-input100"></span> -->
<!-- 							<span class="symbol-input100"> <i class="fa fa-envelope" -->
<!-- 								aria-hidden="true"></i> -->
<!-- 							</span> -->
<!-- 						</div> -->

						<div class="wrap-input100 validate-input"
							data-validate="Password is required">
							<input class="input100" type="password" name="pass"
								placeholder="Password"> <span class="focus-input100"></span>
							<span class="symbol-input100"> <i class="fa fa-lock"
								aria-hidden="true"></i>
							</span>
						</div>

						<div class="container-login100-form-btn">
							<button class="login100-form-btn">Login</button>
						</div>
						
						

						<div class="text-center p-t-12">
							<span class="txt1"> Forgot </span> <a class="txt2" href="#">
								Username / Password? </a>
						</div>

						<div class="text-center p-t-136">
							<a class="txt2" href="${pageContext.request.contextPath}/pages/reg.jsp"> Create your Account <i
								class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
							</a>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- logo -->
		<!-- <div id="logo_wrapper"> -->
		<!--   <a href="http://www.dkos.at" target="_blank"> -->
		<%--     <img src="${pageContext.request.contextPath}/webresources/images/logo.png" border="0" alt="logo" /> --%>
		<!--   </a> -->
		<!-- </div> -->


		<div class="footer">Copyright @ dkos, 2018</div>
	</div>

	<!--===============================================================================================-->
	<script
		src="${pageContext.request.contextPath}/webresources/Login/vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script
		src="${pageContext.request.contextPath}/webresources/Login/vendor/bootstrap/js/popper.js"></script>
	<script
		src="${pageContext.request.contextPath}/webresources/Login/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script
		src="${pageContext.request.contextPath}/webresources/Login/vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script
		src="${pageContext.request.contextPath}/webresources/Login/vendor/tilt/tilt.jquery.min.js"></script>
	<script>
		$('.js-tilt').tilt({
			scale : 1.1
		})
	</script>
	<!--===============================================================================================-->
	<script
		src="${pageContext.request.contextPath}/webresources/Login/js/main.js"></script>

</body>
</html>