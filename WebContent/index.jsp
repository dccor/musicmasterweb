<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MusicMasterWeb</title>
<!-- -------------------------------- -->
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32"
	href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16"
	href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<!-- -- favicon static : -- -->
<link rel="icon" type="image/png"
	href="webresources/images/icons/favicon.ico" />

<!-- All css link -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/webresources/css/styles.css">
</head>


<body>
	<div class="overlay">

		<div id="header">
			<h1 class="h1">
				<b>Welcome to Music Master</b>
			</h1>
			<hr />
		</div>

		<h2>developing stage ...</h2>

		<!-- submit form for Login.jsp link -->
		<form action="pages/login.jsp">
			<input type="submit" value="to login" />
		</form>


		<form action="MyServlet">
			<input type="submit" value="Start" />
		</form>

		<!-- logo -->
		<!-- <img src="${pageContext.request.contextPath}/webresources/images/logo.png" /> -->
		<div id="logo_wrapper">
			<a href="http://www.dkos.at" target="_blank"> <img
				src="${pageContext.request.contextPath}/webresources/images/logo.png"
				border="0" alt="logo" />
			</a>
		</div>
	</div>

	<div class="footer">Copyright @ dkos, 2018</div>

</body>

</html>