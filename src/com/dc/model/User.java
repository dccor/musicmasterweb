package com.dc.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * User Class is @DatabaseTable "users"
 * @author D.Kos
 *
 */
@DatabaseTable(tableName = "users")
public class User {

	/** auto generated Id incremented */
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(canBeNull = false, unique = true)
	private String username;
	@DatabaseField(canBeNull = false)
	private String email;
	@DatabaseField(canBeNull = false)
	private String password;
	@DatabaseField(canBeNull = false, dataType=DataType.BYTE_ARRAY)
	private byte[] salt;
	@DatabaseField(canBeNull = true)
	private Instrument instrument;
	@DatabaseField(canBeNull = false)
	private int sessionCounter;
	@DatabaseField(canBeNull = false)
	private long totalLessionTime;
	@DatabaseField(canBeNull = false)
	private long totalSessionTime;


	

	public User() {
		/** ORMLite needs a no-arg constructor */
	}

	public User(String username, String email, String password, byte[] salt, Instrument instrument, long totalLessionTime, long totalSessionTime) {
		this.username = username;
		this.email = email;
		this.password = password;
		this.salt = salt;
		this.sessionCounter = 1;
		this.instrument = instrument;
		this.totalLessionTime = totalLessionTime;
		this.totalSessionTime = totalSessionTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	public int getSessionCounter() {
		return sessionCounter;
	}

	public void setSessionCounter(int sessionCounter) {
		this.sessionCounter = sessionCounter;
	}
	
	public byte[] getSalt() {
		return salt;
	}
	
	
	public void setSalt(byte[] salt) {
		this.salt = salt;
	}
	
	public long getTotalLessionTime() {
		return totalLessionTime;
	}

	public void setTotalLessionTime(long totalLessionTime) {
		this.totalLessionTime = totalLessionTime;
	}
	
	public long getTotalSessionTime() {
		return totalSessionTime;
	}

	public void setTotalSessionTime(long totalSessionTime) {
		this.totalSessionTime = totalSessionTime;
	}

}
