package com.dc.model;

import java.util.ArrayList;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * SongProperty Class set from Song-Class
 * @author D.Kos
 *
 */
public class SongProperties {

	private StringProperty id = new SimpleStringProperty("");
	private StringProperty title = new SimpleStringProperty("");
	private StringProperty artist = new SimpleStringProperty("");
	private StringProperty genre = new SimpleStringProperty("");
	private StringProperty notePageChangeTimes = new SimpleStringProperty("");
	private IntegerProperty difficulty = new SimpleIntegerProperty();
	private LongProperty length = new SimpleLongProperty();
	
	public SongProperties(Song song) {
		this.setId(song.getId());
		this.setTitle(song.getTitle());
		this.setArtist(song.getArtist());
		this.setGenre(song.getGenre());
		this.setNotePageChangeTimes(song.getNotePageChangeTimes());
		this.setDifficulty(song.getDifficulty());
		this.setLength(song.getLength());;
	}

	
	public StringProperty idProperty() {
        return id;
	}
	public String getId() {
		return id.get();
	}
	public void setId(String id) {
		this.id.set(id);
	}
	
	public StringProperty titleProperty() {
        return title;
	}
	public String getTitle() {
		return title.get();
	}
	public void setTitle(String title) {
		this.title.set(title);
	}
	
	public StringProperty artistProperty() {
        return artist;
	}
	public String getArtist() {
		return artist.get();
	}
	public void setArtist(String artist) {
		this.artist.set(artist);
	}
	
	public StringProperty genreProperty() {
        return genre;
	}
	public String getGenre() {
		return genre.get();
	}
	public void setGenre(String genre) {
		this.genre.set(genre);
	}
	
	public StringProperty notePageChangeTimesProperty() {
        return notePageChangeTimes;
	}
	public String getNotePageChangeTimes() {
		return notePageChangeTimes.get();
	}
	public void setNotePageChangeTimes(String notePageChangeTimes) {
		this.notePageChangeTimes.set(notePageChangeTimes);
	}
	
	public void setNotePageChangeTimes(ArrayList<Integer> array){
		StringBuilder b = new StringBuilder();
		for (int i : array) {
			b.append(Integer.valueOf(i).toString() + ";");
		}
		this.notePageChangeTimes.set(b.toString());
	}
	
    public IntegerProperty difficultyProperty() {
        return difficulty;
    }
	public int getDifficulty() {
		return difficulty.get();
	}
	public void setDifficulty(int difficulty) {
		this.difficulty.set(difficulty);
	}
	
	
	public LongProperty lengthProperty(){
		return length;
	}
	public long getLength() {
		return length.get();
	}
	public void setLength(long length) {
		this.length.set(length);
	}
	

}
