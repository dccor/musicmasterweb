package com.dc.musicmaster;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


import org.musicmaster.views.EditUserdataController;
import org.musicmaster.views.MainViewController;
import org.musicmaster.views.MediaplayerController;
import org.musicmaster.views.RegisterDialogController;
import org.musicmaster.views.StartScreenController;
import org.musicmaster.views.StatisticsController;

import com.dc.model.Song;
import com.dc.model.User;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.DialogPane;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class App extends Application {

	
	private static App instance;
	private Db db;
	private MediaplayerController mpcontroller;
	private MainViewController mvcontroller;
	private StartScreenController sscontroller;
	private StatisticsController statcontroller;
	
	private long startSessionTime;
	
	/** the currentUser from User Class */
	private User currentUser;
	
	/** JavaFX stage and scene */
	public Stage stage;
	public Scene scene;
	
	
	/** static instance of this Class*/
	public static App getInstance() {
		return instance;
	}

	/** new Alert Dialog for Error Messages
	 * @param msg
	 */
	public void showAlert(String msg) {
		Alert alert = new Alert(AlertType.WARNING);
		DialogPane dialogPane = alert.getDialogPane();
		dialogPane.getStylesheets().add(
		   getClass().getResource("views/DarkTheme.css").toExternalForm());
		alert.initOwner(this.stage);
		alert.setResizable(false);
		alert.setTitle("Error Message");
		alert.setHeaderText("Music Master v1.0");
		alert.setContentText(msg);
		alert.showAndWait();
	}

	/**
	 * getter for Songs-Library Path (assets Folder)
	 * @param subPath
	 * @return path
	 */
	public String getAssetPath(String subPath){
		String path = "./assets" + subPath;
		File f = new File(path);
		if (!f.exists()) {
			showAlert("Asset path does not exist! Please check your songs Folder:" + path);
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Asset path does not exist:." + path);
		}
		return path;
	}

	
	/** Override Application start */
	@Override
	public void start(Stage stage)  {
		try {
			@SuppressWarnings("unused")
			String cp = App.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			@SuppressWarnings("unused")
			String p = getAssetPath("/songs/default/song.mid");
		} catch (Exception e1) {
			showAlert("Could not open assets path. Please check 'default' Folder. ");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not open default assets path.", e1);
		}
		this.stage = stage;
		scene = new Scene(new AnchorPane());
		stage.setScene(scene);

		stage.setTitle("MusicMaster v1.0");
		stage.getIcons().add(new Image(getClass().getResourceAsStream("/img/musicmasterkeysicon.jpg")));
		stage.setMinWidth(850);
		stage.setMinHeight(650);
		stage.setWidth(900);
		stage.setHeight(700);
		
		showStartScreen();
		stage.show();
		
		/** create sqlite File */
		db = new Db("database.sqlite");
		try {
			db.open();
		} catch (Exception e) {
			showAlert("Could not load Database (Please contact Administration)");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not open database", e);
		}
	}
	
	/** Override Application Class stop - logoutUser */
	@Override
	public void stop(){
	    logoutUser();
	}

	/** load StartScreen scene */
	public void showStartScreen() {
		instance = this;
		FXMLLoader loader = null;
		try {
			loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("views/startscreen.fxml"));
			scene.setRoot((Parent) loader.load());
		} catch (Exception ex) {
			showAlert("Could not load Start Screen");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not find views/startscreen.fxml", ex);
		}
	}

	/** load MainView scene */
	public void showMainView() {
		instance = this;
		FXMLLoader loader = null;
		try {
			loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("views/mainView.fxml"));
			scene.setRoot((Parent) loader.load());
		} catch (IOException ex) {
			showAlert("Could not load Main Window.");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not find views/mainView.fxml", ex);
		}
		mvcontroller = loader.<MainViewController>getController();
	}

	/**
	 * load mediaPlayer scene, get Controller and run init(song)
	 * @param song
	 */
	public void showMediaplayer(Song song) {
		instance = this;
		FXMLLoader loader = new FXMLLoader();
		try {
			loader.setLocation(App.class.getResource("views/mediaplayer.fxml"));
			scene.setRoot((Parent) loader.load());
		} catch (IOException ex) {
			showAlert("Could not load Media Player Screen.");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not find views/mediaplayer.fxml", ex);
		}
		MediaplayerController mcontroller = loader.<MediaplayerController>getController();
		this.mpcontroller = mcontroller;
		try {
			mcontroller.init(song);
		} catch (Exception e) {
			showAlert("Could not open Media Player." + e.toString());
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not find media player. ", e);
		}
	}
	

	/**
	 *  load showRegisterDialog 
	 *  get UserData and set
	 * @param user
	 * @return
	 */
	public User showRegisterDialog(User user) {
		instance = this;
		try {
			/** Load the fxml file and create a new stage for the dialog. */
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("views/registerDialog.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("User registration");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(this.stage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.setMinWidth(400);
			dialogStage.setMinHeight(500);

			/** get controller set Stage */
			RegisterDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);

			/** for jar export */
//			dialogStage.getIcons().add(new Image(getClass().getResourceAsStream("../../img/musicmasterkeysicon.jpg")));
			dialogStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/musicmasterkeysicon.jpg")));
			dialogStage.showAndWait();

			if (controller.isOkClicked()) {
				return user;
			} else
				return null;
		} catch (IOException e) {
			showAlert("Could not load Register User Dialog!" + e.toString());
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not open RegisterDialog!", e);
			return null;
		}
	}
	
	
	/** load showEditUserdata Dialog
	 * @param user
	 * @return user
	 */
	public User showEditUserdata(User user){
		FXMLLoader loader = null;
		try {
			loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("views/edituserdata.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit Userdata");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(this.stage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.setMinWidth(400);
			dialogStage.setMinHeight(500);

			/** get the Controller set Stage */
			EditUserdataController controller = loader.getController();
			controller.setDialogStage(dialogStage);

			dialogStage.getIcons().add(new Image(getClass().getResourceAsStream("/img/musicmasterkeysicon.jpg")));
			dialogStage.showAndWait();

			if (controller.isOkClicked()) {
				return user;
			} else
				return null;
		} catch (IOException e) {
			showAlert("Could not load Edit Userdata Dialog." + e.toString());
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not open EditUserdata Dialog.", e);
			return null;
		}
	}

	
	/** load Statistics scene */
	public void showStatistics() {
		instance = this;
		FXMLLoader loader = null;
		try {
			loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("views/statistics.fxml"));
			scene.setRoot((Parent) loader.load());
		} catch (Exception ex) {
			showAlert("Could not load Statistic Screen");
			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not find views/statistics.fxml", ex);
		}
		StatisticsController controller = loader.<StatisticsController>getController();
		this.statcontroller = controller;
	}
	
	/**
	 * Convert milliseconds to mm:ss format.
	 * @param millis
	 * @return formated String
	 */
	public static String prettyPrintMilliSeconds(long millis) {
		/** convert and format String time to min:sek */
		long minutes = Math.floorDiv(millis, 60000);
		long seconds = (millis % 60000) / 1000;
		String len = "";
		len += Long.toString(minutes) + ":";
		if (seconds < 10) {
			len += "0";
		}
		len += Long.toString(seconds);
		len += "s";
		return len;
	}
	
	/** sets the stage Fullscreen */
	public void setStageFullScreen(){
		this.stage.setFullScreen(true);
	}
	public void setStageFullScreenfalse(){
		this.stage.setFullScreen(false);
	}
	
	/**  getter for Db Class */
	public Db getDb() {
		return db;
	}
	
	/** get MediaplayerController*/
	public MediaplayerController getMpcontroller() {
		return mpcontroller;
	}
	/** get mainViewController*/
	public MainViewController getMvcontroller() {
		return mvcontroller;
	}
	
	/** get StartScreenController*/
	public StartScreenController getSscontroller() {
		return sscontroller;
	}
	/** get StatisticsController*/
	public StatisticsController getStatcontroller() {
		return statcontroller;
	}
	
	/** accessors for the current User*/
	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}
	public User getCurrentUser(){
		return this.currentUser;
	}
	
	/** setCurrentUser null, run finishSession() */
	public void logoutUser() {
		if(currentUser != null){
		getDb().finishSession(getCurrentUser(), getStartSessionTime());
		setCurrentUser(null);
		}
	}
	
	
	/** accessors for StartSessionTime */
	public long getStartSessionTime() {
		return startSessionTime;
	}
	public void setStartSessionTime(long startSessionTime) {
		this.startSessionTime = startSessionTime;
	}

	
	public static void main(String[] args) {
		launch(args);
	}

	
	
}
