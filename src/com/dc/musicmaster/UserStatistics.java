package com.dc.musicmaster;

import java.util.HashMap;

/**
 * this Class holds the UserStatistic Members
 * Constructor with Attributes
 * @author D.Kos
 *
 */
public class UserStatistics {
	
	public HashMap<String, Integer> genreCounts;
	public long lessonCount;
	
	public UserStatistics(HashMap<String, Integer> genreCounts, long lessonCount) {
		this.genreCounts = genreCounts;
		this.lessonCount = lessonCount;
	}
}
