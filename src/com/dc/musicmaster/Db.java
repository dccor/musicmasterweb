package com.dc.musicmaster;

import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.dc.model.Instrument;
import com.dc.model.Song;
import com.dc.model.Statistics;
import com.dc.model.User;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Database Helper Class
 * ormlite and sqlite
 * @author D.Kos
 *
 */
public class Db {
	
	// dbPath: DBNAME.sqlite
	@SuppressWarnings("unused")
	private String dbPath;
	// MEMO: http://ormlite.com/javadoc/ormlite-core/doc-files/ormlite.html
	private ConnectionSource connection;
	// Dao<Object, Objekt as PK (no primitive Types)
	private Dao<User, Integer> userTable;
	private Dao<Song, String> songTable;
	private Dao<Statistics, Integer> statisticsTable;

	/**
	 * public default Constructor for jsp
	 */
	public Db() {
		
	}
	/**
	 * Constructor
	 * @param dbPath
	 */
	public Db(String dbPath) {
		this.dbPath = dbPath;
	}
	
	/**
	 *  Test for jsp out. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * @return
	 */
	public String testMethod(){
	    return "Hello - i am a Database";
	}

	/** new JdbcConnectionSource
	 *  creates Tables if not exist, if no Song exist - buildDefaultData,
	 */
	public void open() {
		/** this uses h2 by default but change to match your database. */
//		String databaseUrl = "jdbc:sqlite:database.sqlite";
		String databaseUrl = "jdbc:mysql://localhost:3306/musicmaster";
		
		
		/** create a connection source to our database. */
		try {
			connection = new JdbcConnectionSource(databaseUrl);
			/** Create table if it does not exist yet. */
			TableUtils.createTableIfNotExists(connection, User.class);
			TableUtils.createTableIfNotExists(connection, Song.class);
			TableUtils.createTableIfNotExists(connection, Statistics.class);
			/** instantiate the DAO. */
			userTable = DaoManager.createDao(connection, User.class);
			songTable = DaoManager.createDao(connection, Song.class);
			statisticsTable = DaoManager.createDao(connection, Statistics.class);
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not get connection to database and create entries");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not get connection to database and create entries.", e);
		}

		/** buildDefaultData if no Song in Table */
		if (this.countSongs() < 1) {
			this.buildDefaultData();
		}
	}

	/**
	 * create new User and write to userTable
	 * create Songs and write to songTable
	 */
	public void buildDefaultData() {
//		 createUser(new User("test", "Vorname", "Nachname", "test@test.com",
//		 "test", getSalt(), Instrument.GUITAR));

		Song[] songs = {
				new Song("clapton_eric_signe", "Signe", "Eric Clapton", "Blues", 0, 1, new int[] { 16, 66, 104 }),
				new Song("johnson_robert_me_and_the_devil_blues", "Me And The Devil Blues", "Robert Johnson", "Blues", 0, 1,
						new int[] { 33 }),
				new Song("bbking_how_blue_can_you_get", "How blue can you get", "B.B. King", "Jazz", 0, 3,
						new int[] { 72, 154, 223, 282 }),
				new Song("miles_davis_so_what", "So what", "Miles Davis", "Jazz", 0, 3,
						new int[] { 36, 74, 112, 160, 204, 239, 291, 332, 394, 496, 509 }),
				new Song("deep_purple_perfect_strangers", "Perfect Strangers", "Deep Purple", "Rock", 0, 1,
						new int[] { 66, 145, 269 }),
				new Song("hendrix_jimi_manic_depression", "Manic Depression", "Jimi Hendrix", "Rock", 0, 3,
						new int[] { 36, 64, 116, 150, 212 }),
				new Song("paco_de_lucia_tempul", "Tempul", "Paco De Lucia", "Flamenco", 0, 3,
						new int[] { 20, 41, 62 }),
				new Song("pantera_strenght_beyond_strenght", "Strenght Beyond Strenght", "Pantera", "Heavy Metal", 0, 2,
						new int[] { 62, 126, 178 }),
		
		
		};
			
		for (Song song : songs) {
			song.setLength(MidiPlayer.determineLength("assets/songs/" + song.getId() + "/song.mid"));
			createSong(song);
		}
	}

	/**
	 * load User by username and set User
	 * @param username
	 * @return user
	 * @throws SQLException
	 */
	public User loadUserByName(String username) {
		QueryBuilder<User, Integer> qb = userTable.queryBuilder();
		User user;
		try {
			qb.where().eq("username", username);
			user = qb.queryForFirst();
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not load User from database.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not load UserByName.", e);
			return null;
		}
		return user;
	}

	/**
	 * load UserByName, authenticateUser: generate password hash with salt
	 * @param username, password
	 * @return user
	 */
	public User authenticateUser(String username, String password) {
		User user;
		try {
			user = loadUserByName(username);
		} catch (Exception e) {
//			App.getInstance().showAlert("Could not load from Database.");
			return null;
		}
		if (user == null) {
			return null;
		}
		if (!PasswordGenerator.validatePassword(password, user.getSalt(), user.getPassword())) {
			return null;
		}
		return user;
	}


	
	/** calculate SUM of all Songs played for each User
	 * @param userId
	 * @return songs length
	 * @throws Exception
	 */
	public long getLessionTimeForUser(int userId) {
		String query = "SELECT SUM(`songs`.length) FROM `statistics` INNER JOIN `songs` ON `statistics`.`song_id` = `songs`.`id` WHERE `statistics`.`user_id` = "
				+ userId;
		String rawSum;
		try {
			rawSum = statisticsTable.queryRaw(query).getFirstResult()[0];
			long sum = Long.parseLong(rawSum);
			return sum;
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not get Lession Time.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not get Lession Time from statistics table.", e);
			return -1;
		}
	}
	
	/**
	 * add SongStatistic with stats from sql 
	 * JOIN query (song.title, song.artist, song.genre, statistics.time) for userId
	 * @param userId
	 * @return ArrayList<SongStatistic>
	 */
	public ObservableList<SongStatistics> getSongsPlayedByUser(int userId) {
		String query = "SELECT so.title, so.artist, so.genre, st.time FROM statistics st INNER JOIN songs so ON st.song_id = so.id WHERE st.user_id = " + userId;
		 try {
			List<String[]> rows = statisticsTable.queryRaw(query).getResults();
			
			ObservableList<SongStatistics> stats = FXCollections.observableArrayList();
			 ZoneOffset offset = ZoneId.systemDefault().getRules().getOffset(Instant.now());
			 for (String[] columns : rows) {
				 LocalDateTime dt = LocalDateTime.ofEpochSecond(Long.parseLong(columns[3]), 0, offset);
				 stats.add(new SongStatistics(columns[0], columns[1], columns[2], dt));
			 }
			 return stats;
		} catch (NumberFormatException e) {
//			App.getInstance().showAlert("Could not get Songs Statistics from database.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not get Songs Statistics from database.", e);
			return null;
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not get Songs Statistics from database.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not get Songs Statistics from database.", e);
			return null;
		}
	}

	/** creates a new User in Table */
	public void createUser(User user)  {
		if (loadUserByName(user.getUsername()) != null) {
//....FROM OLD 
//			App.getInstance().getSscontroller().setMessage("Username already exists.");
		}
		try {
			userTable.create(user);
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not create User.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not create User.", e);
		}
	}

	/** update user in userTable */
	public void updateUser(User user) {
		try {
			userTable.update(user);
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not get update Userdata in database.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not update User.", e);
		}
	}

	/** returns count of Song entries from songTable */
	public long countSongs() {
		try {
			return songTable.countOf();
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not get Songcount from database.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not get Songcount from songtable.", e);
			return -1;
		}
	}

	/**
	 * set TotalLessionTime, update user and write to statisticsTable
	 * @param user
	 * @param song
	 */
	public void createUserStatisticsEntry(User user, Song song) {
		user.setTotalLessionTime(user.getTotalLessionTime() + song.getLength());
		try {
			userTable.update(user);
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not update User.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not update User.", e);
		}
		ZoneId zoneId = ZoneId.systemDefault();
		try {
			statisticsTable.create(new Statistics(user, song, LocalDateTime.now().atZone(zoneId).toEpochSecond()));
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not create Statistics database entry.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not create statistics table.", e);
		}
	}

	/** create a new Song in songTable */
	public void createSong(Song song) {
		try {
			songTable.create(song);
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not create Song.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not create Song.", e);
		}
	}

	/** load Song by id (selectedSong)*/
	public Song loadSongById(String id){
		try {
			return songTable.queryForId(id);
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not load Song.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not load Song.", e);
			return null;
		}
	}

	/** load Song List query*/
	public List<Song> loadSongList()  {
		try {
			return songTable.queryForAll();
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not load Song List.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not load Song List.", e);
			return null;
		}	
	}

	/**
	 * for each Statistics entry from UserId get Genre (and add + 1 Genre) from
	 * statisticsTable
	 * @param userId
	 * @return map
	 */
	public UserStatistics getUserGenreStatistics(int userId) {
		HashMap<String, Integer> stats = new HashMap<String, Integer>();
		String query = "SELECT songs.genre FROM `statistics` INNER JOIN `songs` ON `statistics`.`song_id` = `songs`.`id` WHERE `statistics`.`user_id` = "
				+ userId;

		List<String[]> rows;
		try {
			rows = statisticsTable.queryRaw(query).getResults();
		for (String[] fields : rows) {
			String genre = fields[0];
			if (stats.containsKey(genre)) {
				stats.put(genre, stats.get(genre) + 1);
			} else {
				stats.put(genre, 1);
			}
		}
		return new UserStatistics(stats, rows.size());
		
		} catch (SQLException e) {
//			App.getInstance().showAlert("Could not get Genre Statistics.");
//			Logger.getLogger(App.class.getName()).log(Level.SEVERE, "Could not get User Genre Statistics.", e);
			return null;
		}
	}


	/** set the SessionCounter calculate and set totalSessionTime and updateUser
	 * @param currentuser
	 * @param sessionStartTime
	 */
	public void finishSession(User currentuser, long sessionStartTime){
		currentuser.setSessionCounter(currentuser.getSessionCounter() + 1);
		long st = currentuser.getTotalSessionTime() + (Instant.now().toEpochMilli() - sessionStartTime);
		currentuser.setTotalSessionTime(st);
		updateUser(currentuser);
	}

}
